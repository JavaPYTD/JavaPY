package com.xiaoyao.utils;

        import com.alibaba.fastjson.JSONObject;

        import java.text.ParseException;
        import java.text.SimpleDateFormat;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: DataProcesS
 * @Description: TODO(Json数据封装处理类)
 * @Author: WDD
 * @Date: 2020/2/26 20:44
 */
@FunctionalInterface //函数式接口注解
public interface JsonDataProcess {
    /**
     * 该方法是将Json数据上传到数据库中时，对Json对象的数据封装操作
     * @param obj json对象
     * @param sdf 时间格式化对象
     * @throws ParseException
     */
    void run(JSONObject obj, SimpleDateFormat sdf) throws ParseException;
}
