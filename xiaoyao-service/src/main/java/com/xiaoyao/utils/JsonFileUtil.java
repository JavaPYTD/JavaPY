package com.xiaoyao.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xiaoyao.opjo.model.DynamicInformation;
import com.xiaoyao.opjo.model.PolularScience;
import com.xiaoyao.opjo.model.Rumor;
import com.xiaoyao.service.DynamicInformationService;
import com.xiaoyao.service.PolularScienceService;
import com.xiaoyao.service.RumorService;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
/**
 * @version v1.0
 * @ProjectName: xiaoyao-parent
 * @ClassName: JsonTest
 * @Description: TODO(测试)
 * @Author: WDD
 * @Date: 2020/2/26 16:50
 *  该工具类设计目的:
 *      将Json文件中的数据提取出来并保存到数据库中，方法可扩展
 *  例子：
 *      public static void aaBbDdToDataBase(AaBbDdService service,String jsonFile) throws ParseException{
 *           JsonEntityToDataBase((obj, sdf) -> {
 *              //obj为JSONObject,sdf 时间格式类
 *              //封装json实体
 *              //server.add(实体);
 *           },jsonFile);
 *      }
 *      json文件需要放在资源文件夹根目录
 *
 */
public class JsonFileUtil {

    /*
    * {"r_release":"02-25 16:23",
    * "r_context":"流传内容：位于南礼士路的北京建工发展大厦，有员工老婆确认为新冠肺炎，引发整栋楼居家隔离。要点：1.事件发展具体经过北京建工发展大厦接到驻厦租户中国纸业投资有限公司的报告，称该公司员工王某的妻子被确诊为新冠肺炎。具体经过如下：2月13日，该公司疫情防控领导小组办公室接到王某报告，其妻子携女儿1月25日从湖北荆州返京，王某驾车接站返回家中。自2月7日起王某妻子有咳嗽症状。2月12日晚咳嗽症状未见减轻，前往医院检查，王某妻子核酸检测阴性，但CT胸片显示肺部见阴影，留院观察。王某和女儿两项检测均未见异常，返回家中隔离观察。1月24日至2月12日，王某多次到北京建工发展大厦办公。期间，中国纸业投资有限公司多人多次询问王某及其家属是否返乡探亲，王某均反馈不曾离京去往湖北疫区。2.北京建工发展大厦应对措施北京建工发展大厦物业单位协同各驻厦单位迅速应对。所有驻厦单位暂停办公，所有单位人员居家自行隔离，每日上报体温监测情况和身体状况。",
    * "r_conclusion":"鉴定结果：证实",
    * "r_page_src":"https://mbd.baidu.com/newspage/data/landingshare?context=%7B%22nid%22%3A%22news_9426275040648880135%22%2C%22sourceFrom%22%3A%22bjh%22%7D&pageType=1",
    * "r_info_src":"['北京晚报']",
    * "r_img_src":"https://f11.baidu.com/it/u=2428882243,3153051037&fm=173&app=49&f=JPEG?w=300&h=219&s=162247A072C61AEEF03095B5030050F1&access=215967316",
    * "r_status":"",
    * "r_title":"北京建工发展大厦全部人员居家隔离？"}
    */
    /**
     * 谣言json文件到数据库
     * @param service
     * @param jsonFile
     * @throws ParseException
     */
    public static void rumorToDataBase(RumorService service,String jsonFile) throws ParseException {
        JsonEntityToDataBase((obj, sdf) -> {
            //由于时间格式不对，需要自定时间格式
            sdf = new SimpleDateFormat("MM-dd HH:mm");
            Rumor rumor = new Rumor();
            rumor.setReleaseTime(sdf.parse(obj.getString("r_release")));
            rumor.setrContext(obj.getString("r_context"));
            rumor.setrConclusion(obj.getString("r_conclusion"));
            rumor.setrInfoSrc(obj.getString("r_page_src"));
            String r_info_src = obj.getString("r_info_src");
            if("[]".equals(r_info_src)){
                r_info_src="";
            }else {
                //1.取消[]
                r_info_src = r_info_src.substring(r_info_src.indexOf("[") + 1, r_info_src.indexOf("]")).split(",")[0];
                //2.
                if ("''".equals(r_info_src)||"'</span>'".equals(r_info_src)){
                    r_info_src = "";
                }else {
                    r_info_src = r_info_src.substring(r_info_src.indexOf("'") + 1, r_info_src.lastIndexOf("'"));
                }
            }
            rumor.setrInfoFrom(r_info_src);
            rumor.setrImgSrc(obj.getString("r_img_src"));
            rumor.setrTitle(obj.getString("r_title"));
            service.add(rumor);
        },jsonFile);
    }

    /*
    *   {"ps_img_video_src":"http://www.nhc.gov.cn/xcs/kpzs/list_gzbd/97a57aaa84804b45a520330a4289bbfe/images/285380c14e16420ab608a7ce8dd97ae0.jpg\n http://www.nhc.gov.cn/xcs/kpzs/list_gzbd/97a57aaa84804b45a520330a4289bbfe/images/2160647375fe43f88fe67f262c3902b0.jpg\n http://www.nhc.gov.cn/xcs/kpzs/list_gzbd/97a57aaa84804b45a520330a4289bbfe/images/48a02d2a978e4d29acfec04c4a026bcd.jpg\n http://www.nhc.gov.cn/xcs/kpzs/list_gzbd/97a57aaa84804b45a520330a4289bbfe/images/b1245149dc604cc89dde36029d03e48f.jpg\n http://www.nhc.gov.cn/xcs/kpzs/list_gzbd/97a57aaa84804b45a520330a4289bbfe/images/ec2d9c323d4142ac9a745cfa62f863b9.jpg\n http://www.nhc.gov.cn/xcs/kpzs/list_gzbd//default/images/icon16/rar.gif\n ",
    *   "ps_context":"\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n附件：\r\n  　　相关链接：1.　　　　　　　2.\r\n",
    *   "info_from":[],
    *   "ps_title":"一图读懂：新型冠状病毒肺炎防控方案（第四版）",
    *   "ps_info_src":"http://www.nhc.gov.cn/xcs/kpzs/202002/97a57aaa84804b45a520330a4289bbfe.shtml",
    *   "release_time":"2020-02-13 16:12"}
    */
    /**
     * 科普json文件保存至数据库
     * @param service
     * @param jsonFile
     * @throws ParseException
     */
    public static void polularScienceToDataBase(PolularScienceService service,String jsonFile) throws ParseException {
        JsonEntityToDataBase((obj, sdf) -> {
            PolularScience ps = new PolularScience();
            ps.setPsImgSrc( obj.getString("ps_img_video_src"));
            String ps_context = obj.getString("ps_context")
                    .replace("（）"," ")
                    .replace(" ","");
            ps.setPsContext(ps_context);
            ps.setPsInfoSrc(obj.getString("ps_info_src"));
            ps.setPsTitle(obj.getString("ps_title"));
            String info_from = obj.getString("info_from")
                    .replace("[]","")
                    .replace("[\"","")
                    .replace("\"]","")
                    .replaceAll("[</span>]","")
                    .replaceAll("[<br>]","")
                    .replaceAll("[</p>]","")
                    .replaceAll("[</font>]","")
                    .replaceAll("[&ensp;]","")
                    .replaceAll("[\\\\n]","")
                    .replaceAll("[\\\\r]","")
                    .replaceAll("）","")
                    .replace(")","")
                    .replace("yl=\"FONT-FAMILY: 仿宋,仿宋_GB2312 FONT-SIZE: 16\" yl=\"FONT-FAMILY: 仿宋,仿宋_GB2312 FONT-SIZE: 16\"","")
                    .replace("yl=\"FONT-FAMILY: 仿宋,仿宋_GB2312 FONT-SIZE: 16\"","")
                    .replace("ci yl=\"BOX-SIZING: d-x !im TEXT-ALIGN: c PADDING-BOTTOM: 0x LINE-HEIGHT: 1.75m WIDOWS: 2 TEXT-TRANSFORM:  BACKGROUND-COLOR: g(255,255,255 FONT-STYLE: ml TEXT-INDENT: 0x MARGIN: 0x MIN-HEIGHT: 1m PADDING-LEFT: 0x PADDING-RIGHT: 0x FONT-FAMILY:  MAX-WIDTH: 100% WORD-WRAP: k-wd !im WHITE-SPACE: ml ORPHANS: 2 LETTER-SPACING: 0x FONT-WEIGHT: ml WORD-SPACING: 0x PADDING-TOP: 0x -vi-ligu: ml -vi-c: ml -wki-x-k-widh: 0x vlw-w: k-wd\" yl=\"BOX-SIZING: d-x !im PADDING-BOTTOM: 0x MARGIN: 0x PADDING-LEFT: 0x PADDING-RIGHT: 0x FONT-FAMILY: 仿宋,仿宋_GB2312 MAX-WIDTH: 100% WORD-WRAP: k-wd !im COLOR: #000000 FONT-SIZE: 16 PADDING-TOP: 0x\" yl=\"BOX-SIZING: d-x !im PADDING-BOTTOM: 0x MARGIN: 0x PADDING-LEFT: 0x PADDING-RIGHT: 0x FONT-FAMILY: 仿宋,仿宋_GB2312 MAX-WIDTH: 100% WORD-WRAP: k-wd !im LETTER-SPACING: 0x COLOR: #000000 FONT-SIZE: 16 PADDING-TOP: 0x\" ","")
                    .replace("ci yl=\"FONT-FAMILY: \"","")
                    .replace(" yl=\"-mily: 仿宋,仿宋_GB2312 -iz: 16\"","");
            System.out.println(info_from);
            ps.setPsInfoFrom(info_from);
            ps.setReleaseTime(sdf.parse(obj.getString("release_time")));
            //System.out.println(ps);
            service.add(ps);
        },jsonFile);
    }

    /* {"di_info_src":"http://www.nhc.gov.cn/xcs/fkdt/202002/08f86ed6d1ca43d291bcdbb849598ddc.shtml",
      "info_from":["新华网，2020年02月24日）</p>\r\n"],
      "di_title":"[新华网] 坚定必胜信念 不获全胜决不轻言成功——习近平总书记在统筹推进新冠肺炎疫情防控和经济社会发展工作部署会议上的重要讲话鼓舞人心提振士气",
      "di_context":"新华社北京2月24日电　题：坚定必胜信念 不获全胜决不轻言成功——习近平总书记在统筹推进新冠肺炎疫情防控和经济社会发展工作部署会议上的重要讲话鼓舞人心提振士气\r\n　　新华社记者\r\n　　当前，新冠肺炎疫情形势依然严峻复杂，防控正处在最吃劲的关键阶段。习近平总书记近日出席统筹推进新冠肺炎疫情防控和经济社会发展工作部署会议并发表重要讲话，强调各级党委和政府要坚定必胜信念，咬紧牙关，继续毫不放松抓紧抓实抓细各项防控工作，不获全胜决不轻言成功。\r\n　　奋战在疫情防控一线的广大医务工作者和社会各界表示，习近平总书记的重要讲话激发起同舟共济、共克时艰的磅礴力量，凝聚起不获全胜决不轻言成功的信心和士气，为我们全面打赢疫情防控人民战争、总体战、阻击战注入强大力量。\r\n　　为实现“应收尽收、应治尽治”，连日来，一座座方舱医院在武汉三镇建设启用，大幅扩容收治能力，为夺取疫情防控胜利筑牢基石。\r\n　　武汉大学人民医院副院长、武昌方舱医院院长万军说，通过按照相应诊疗方案科学施治，一批批新冠肺炎轻症患者顺利出院。下一步，我们要认真贯彻落实习近平总书记在统筹推进新冠肺炎疫情防控和经济社会发展工作部署会议上的重要讲话精神，统筹推进各项防控工作，继续加大救治力度，使方舱医院真正成为抗疫前线的“生命之舟”。\r\n　　社区防控是疫情防控的基础环节。在武汉市青山区工人村街青和居社区，党总支书记桂小妹组织开展对社区5235户进行拉网式排查。她说，习近平总书记的重要讲话激励着我们继续抓好防控工作，精准把好社区（村）、单位、家庭和个人防控关口，严格网格化管理，落细落实防控措施。\r\n　　新冠肺炎疫情发生以来，北京市疫情防控领导小组迅速建立医疗保障机制、明确市级定点收治医院、发布疫情防控专业指引、织紧织密社区防控网络。“习近平总书记的重要讲话，令首都卫生健康系统深感责任重大。”北京市卫生健康委党委书记、主任雷海潮说，我们要按照总书记的重要讲话精神，继续咬紧牙关、毫不放松，抓紧抓实抓细各项防控工作。\r\n　　“总书记强调关心关爱一线医务人员，令我们感到暖心和鼓舞。”第五批江苏援湖北医疗队南京一队领队、南京鼓楼医院副院长于成功告诉记者，目前，江苏已有近3000人在湖北进行支援。为加强对一线医务人员的关心关爱，地方从落实工作生活保障、做好心理援助疏导、落实津贴补贴保险待遇等方面给予关怀，让我们在前线抗“疫”没有后顾之忧。\r\n　　在人类与疫病的斗争中，科学始终是最有力的武器，疫情防控进一步取得成效离不开科研工作的持续推进。\r\n　　“综合多学科力量开展科研攻关”“加大药品和疫苗研发力度”……上海市公共卫生临床中心新发与再现传染病研究所所长徐建青说，习近平总书记的重要讲话为加快科技研发攻关指明方向。\r\n　　徐建青表示，他所在的研究所正在与同济大学、上海科技大学以及一些海外实验室等展开合作，共同推进抗病毒疫苗、免疫应答等一系列科研攻关。\r\n　　把人民群众生命安全和身体健康放在第一位，对科研人员而言，就是把论文写在抗击疫情的第一线，把研究成果应用到战胜疫情中。\r\n　　一次性医用口罩如何安全可再生？前线医务人员怎样做好防护？在中国工程院院士、复旦大学教授闻玉梅看来，科研人员不能仅埋首于实验室，还要快速回应社会需求。“我们要认真贯彻落实习近平总书记关于‘加快科技研发攻关’的要求，既要开展病原等基础研究，更要紧密结合临床和社会情况切实解决问题。”\r\n　　快速分离出新型冠状病毒、部分药物初步显示出临床疗效、中西医结合显成效……在抗“疫”战线上，多项科学试验正在同步推进。\r\n　　“任务很艰巨，但我们有决心有信心。”湖北省新冠肺炎应急科研攻关研究专家组成员、华中农业大学金梅林教授团队一直在为抗击疫情加紧科研攻关。从业近40年，金梅林和她的研究团队在人兽共患传染病流行病学、致病机制、新型疫苗与分子诊断制剂研究等方面不断取得新突破。这次疫情发生后，金梅林带领的团队第一时间冲上一线，为探寻疫情来龙去脉，控制病毒蔓延做了大量工作。目前，金梅林和团队正在加快科技研发攻关，为疫情防控提供更多科技支撑。\r\n　　“在接下来的攻关中，要继续突出结果导向，同时调动实验室及各有关研究力量协同攻关。”中国科学院院士、广州再生医学与健康广东省实验室主任徐涛说，越是到关键时刻，越需要各方众志成城、协同攻关。疫情发生后，徐涛所在的实验室紧急攻关，在磷酸氯喹对新冠肺炎的临床疗效评价等方面取得积极进展。\r\n　　与时间赛跑，与病魔竞速。徐涛说：“科研人员要发挥召之即来、来之能战、战之能胜的作用，在国家和民族需要的时刻发挥战斗力。”\r\n　　疫情当前，重要医疗救治设备和物资生产调配刻不容缓。每多生产一个、多采购一个、多转运一个，就为抗击疫情多输送一颗“子弹”。\r\n　　在广西北仑河医疗卫生材料有限公司的无菌车间里，三条自动化生产线昼夜不停，一只只医用口罩批量生产，准备送往抗击疫情一线。\r\n　　“习近平总书记的重要讲话催人奋进，广大职工表示要争分夺秒、保质保量地生产医用口罩等紧缺物资，力争为医疗机构和社会大众提供更多防护保障。”公司总裁助理潘才鹏介绍，20名生产线工作人员每天3班倒，不断提升效率，近一个月已生产医用口罩280多万只。\r\n　　为做好北京生活必需品应急保供，北京市商务局已督促指导北京7家重点农产品批发市场加大货源组织调运，并对重点批发市场、大型连锁商超等建立了“点对点”监测补货保障机制，及时了解市场供应和价格情况。“目前北京市商业服务业中的连锁超市、大型商场、批发市场等开工率均在90%以上，近5000家品牌连锁便利店经营已基本恢复正常。”北京市商务局副局长刘梅英说。\r\n　　2月24日，四川永鑫农牧集团正将一车车猪肉运往四川省资阳市城区十多家商超和农贸市场。“作为一家农业产业化国家重点龙头企业，我们正大力恢复生猪屠宰业务，为抗疫一线和居民生活提供稳定供应。”集团总经理杨启志说。\r\n　　畅通运输通道和物流配送，是解决好生活必需品供应“最后一公里”的重要一环。\r\n　　湖北省交通运输厅厅长朱汉桥表示，将认真贯彻落实习近平总书记重要讲话精神，强化联动协调，强化落实落细，强化高效保通，为疫情防控应急物资和支援人员提供交通运输保障。按照防控工作需要，进一步强化国家、省级保障物资、捐赠物资以及应急物资重点运输保畅工作，打好疫情防控阻击战，打好后勤保障战。\r\n　　作为湘鄂间的重要枢纽，湖南省岳阳市的高速公路近来日益繁忙，货车流量稳步上升。湖南省交通运输厅安全监督处处长汤远华介绍，按照习近平总书记提出的“建立交通运输‘绿色通道’”的要求，我们正在积极落实，通过创新举措、简化审批，除必要的对司机快速体温检测外，确保防疫物资和重要生活生产物资运输车辆不停车、不检查、不收费，优先通行，确保“米袋子”“菜篮子”产品和养殖业饲料等农业生产资料运输高效顺畅。（记者陈聪、孙奕、吴雨、李伟、邱冰清、荆淮侨、黄浩铭、仇逸、侠克、吉宁、史卫燕、张海磊）（来源：新华网，2020年02月24日）",
      "di_img_video_src":"",
      "release_time":"2020-02-25 08:52"}*/
    /**
     * 官方动态json文件保存至数据库
     * @param service
     * @param jsonFile
     * @throws ParseException
     */
    public static void dynamicInformationToDataBase(DynamicInformationService service,String jsonFile) throws ParseException {
        JsonEntityToDataBase((obj,sdf) -> {
            DynamicInformation di = new DynamicInformation();
            di.setDiContext(obj.getString("di_context"));
            di.setDiInfoSrc( obj.getString("di_info_src"));
            di.setDiTitle(obj.getString("di_title"));
            di.setDiImgSrc(obj.getString("di_img_video_src"));
            String info_from = obj.getString("info_from")
                    .replace("[]","")
                    .replaceAll("[</span>]","")
                    .replaceAll("[<br>]","")
                    .replaceAll("[</p>]","")
                    .replaceAll("[</font>]","")
                    .replaceAll("[&ensp;]","")
                    .replaceAll("[\\\\n]","")
                    .replaceAll("[\\\\r]","")
                    .replaceAll("）","")
                    .replace(")","")
                    .replaceAll("\"","")
                    .replaceAll("\"","")
                    .replaceAll(" yl=-mily: 仿宋,仿宋_GB2312 -iz: 16","")
                    .replaceAll(" yl=FONT-FAMILY: 仿宋,仿宋_GB2312 FONT-SIZE: 16","")
                    .replaceAll("[ci]","")
                    .replace(" yl=-mily: 仿宋,仿宋_GB2312 -iz: 16","");
            if(!StringUtils.isEmpty(info_from)){
                info_from = info_from.substring(info_from.indexOf("[")+1,info_from.indexOf("]"));
            }
            System.out.println(info_from);
            di.setDiInfoFrom(info_from);
            //时间格式化
            di.setReleaseTime(sdf.parse(obj.getString("release_time")));
           //service.add(di);
        }, jsonFile);
    }

    /**
     *
     * @param dataProcess 数据封装
     * @param jsonFile  json文件
     * @throws ParseException
     */
    private static void JsonEntityToDataBase(JsonDataProcess dataProcess, String jsonFile) throws ParseException {
        //1.获取Json文件的路径
        String path = "";
        try {
           path = Thread.currentThread().getContextClassLoader().getResource(jsonFile).getPath();
        }catch (Exception e){
            new RuntimeException(jsonFile+"该文件不存在");
        }
        //2.将Json文件内容转化为字符串
        String json = readJsonFile(path);
        //3.获取一个Json数组
        JSONArray array = JSONArray.parseArray(json);
        //4.设置日期的格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        //5.遍历Json数组
        for (int i = 0; i < array.size(); i++) {
            //5.1获取Json对象
            JSONObject obj = (JSONObject)array.get(i);
            //5.2封装Json实体,添加到数据库
            dataProcess.run(obj,sdf);
        }
    }

    /**
     * 读取json文件，返回json串
     * @param fileName
     * @return
     */
    private static String readJsonFile(String fileName) {
        String jsonStr = "";
        try {
            File jsonFile = new File(fileName);
            FileReader fileReader = new FileReader(jsonFile);

            Reader reader = new InputStreamReader(new FileInputStream(jsonFile),"utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
