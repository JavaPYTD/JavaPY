package com.xiaoyao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-parent
 * @ClassName: XiaoYaoServiceApplication
 * @Description: TODO(一句话描述该类的功能)
 * @Author: WDD
 * @Date: 2020/2/24 16:30
 */
@SpringBootApplication
@EnableSwagger2
public class XiaoYaoServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaoYaoServiceApplication.class,args);
    }
}
