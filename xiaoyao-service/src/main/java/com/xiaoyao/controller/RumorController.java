package com.xiaoyao.controller;

import com.xiaoyao.opjo.model.Rumor;
import com.xiaoyao.service.RumorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: RumorController
 * @Description: TODO(RumorController)
 * @Author: WDD
 * @Date: 2020/2/27 1:44
 */
@RestController
@Api(value = "Rumor控制层",description = "谣言接口管理")
public class RumorController {


    @Autowired
    private RumorService service;

    @ApiOperation("获取谣言列表")
    @GetMapping("/rumor/list")
    public List<Rumor> list(){
        return service.findAll();
    }

}
