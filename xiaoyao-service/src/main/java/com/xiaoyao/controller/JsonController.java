package com.xiaoyao.controller;

import com.xiaoyao.service.DynamicInformationService;
import com.xiaoyao.service.PolularScienceService;
import com.xiaoyao.service.RumorService;
import com.xiaoyao.utils.JsonFileUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: JsonController
 * @Description: TODO(Json文件导入数据库)
 * @Author: WDD
 * @Date: 2020/2/26 20:05
 */
@RestController
@Api(value = "Json工具控制层",description = "该接口为Json数据文件上传接口，不必在意")
public class JsonController {

    @Autowired
    private DynamicInformationService service;
    @Autowired
    private PolularScienceService scienceService;
    @Autowired
    private RumorService rumorService;

    @ApiOperation("Json官方动态数据上传")
    @GetMapping("/util/di")
    public  boolean di(@ApiParam("json文件名->diTest0x.json") String jsonFile) throws ParseException {
        JsonFileUtil.dynamicInformationToDataBase(service,jsonFile);
        return true;
    }

    @ApiOperation("Json科普数据上传")
    @GetMapping("/util/ps")
    public boolean ps(@ApiParam("json文件名->PsTest0x.json") String jsonFile) throws ParseException {
        JsonFileUtil.polularScienceToDataBase(scienceService,jsonFile);
        return true;
    }

    @ApiOperation("Json谣言数据上传")
    @GetMapping("/util/r")
    public boolean r(@ApiParam("json文件名->rTest0x.json") String jsonFile) throws ParseException {
        JsonFileUtil.rumorToDataBase(rumorService,jsonFile);
        return true;
    }
}
