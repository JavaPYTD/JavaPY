package com.xiaoyao.controller;

import com.alibaba.fastjson.JSONObject;
import com.xiaoyao.service.DynamicInformationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-parent
 * @ClassName: UserController
 * @Description: TODO(UserController)
 * @Author: WDD
 * @Date: 2020/2/24 16:27
 */
@RestController
@ApiModel(value = "User控制层",description = "用户接口管理")
public class UserController {
    @Autowired
    private RestTemplate restTemplate;
    private static final String URL = "https://api.weixin.qq.com/sns/jscode2session";
    private static final String APP_ID = "wx4d194be139c30453";
    private static final String APP_SECRET = "b20e67c6ddba852834784665d1ff9348";

    @ApiOperation("用户登录接口")
    @GetMapping("/login")
    public Map<String, Object> login(@ApiParam("微信端获取的code") String code) {
        Object obj = restTemplate.getForObject(URL+"?appid=" + APP_ID + "&secret=" + APP_SECRET + "&js_code=" + code + "&grant_type=authorization_code", String.class);
        String str = obj.toString();
        JSONObject json = JSONObject.parseObject(str);
        Map<String, Object> map = new HashMap<String, Object>();
        Set<String> keys = json.keySet();
        for (String key : keys) {
            map.put(key, json.get(key));
        }
        return map;
    }


}
