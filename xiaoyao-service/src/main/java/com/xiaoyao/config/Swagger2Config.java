package com.xiaoyao.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-parent
 * @ClassName: Swagger2Config
 * @Description: TODO(Swagger2 配置文件)
 * @Author: WDD
 * @Date: 2020/2/24 16:30
 */
@Configuration
public class Swagger2Config {

    @Value("${swagger.enable}")
    public boolean isEnable;

    @Bean
    public Docket docket(ApiInfo apiInfo){
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(isEnable)
                .apiInfo(apiInfo)
                .groupName("消谣")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.xiaoyao.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public ApiInfo apiInfo(){
        Contact contact = new Contact("TwoKe","TwoKe945@163.com","TwoKe945@163.com");
        return new ApiInfoBuilder()
                .contact(contact)
                .version("v1.0.0")
                .description("消谣微信小程序--->API文档")
                .title("在线API文档")
                .build();
    }

}
