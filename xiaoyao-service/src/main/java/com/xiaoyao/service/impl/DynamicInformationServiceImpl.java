package com.xiaoyao.service.impl;

import com.xiaoyao.mapper.DynamicInformationMapper;
import com.xiaoyao.opjo.model.DynamicInformation;
import com.xiaoyao.service.DynamicInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: DynamicInformationServiceImpl
 * @Description: TODO(DynamicInformationService实现类)
 * @Author: WDD
 * @Date: 2020/2/26 18:35
 */
@Service
public class DynamicInformationServiceImpl implements DynamicInformationService {

    @Autowired
    private DynamicInformationMapper mapper;

    public List<DynamicInformation> findAll() {
        return mapper.findAll();
    }

    public boolean add(DynamicInformation di) {
        di.setCreateBy("SYSTEM");
        di.setUpdateBy("SYSTEM");
        return mapper.add(di);
    }
}
