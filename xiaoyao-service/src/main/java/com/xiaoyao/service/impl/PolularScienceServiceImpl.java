package com.xiaoyao.service.impl;

import com.xiaoyao.mapper.PolularScienceMapper;
import com.xiaoyao.opjo.model.PolularScience;
import com.xiaoyao.service.PolularScienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: PolularScienceServiceImpl
 * @Description: TODO(PolularScienceServiceImpl)
 * @Author: WDD
 * @Date: 2020/2/26 21:30
 */
@Service
public class PolularScienceServiceImpl implements PolularScienceService {

    @Autowired
    private PolularScienceMapper mapper;

    @Override
    public List<PolularScience> findAll() {
        return mapper.findAll();
    }

    @Override
    public boolean add(PolularScience ps) {
        ps.setCreateBy("SYSTEM");
        ps.setUpdateBy("SYSTEM");
        return mapper.add(ps);
    }
}
