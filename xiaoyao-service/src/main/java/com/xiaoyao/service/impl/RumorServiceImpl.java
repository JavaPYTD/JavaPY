package com.xiaoyao.service.impl;

import com.xiaoyao.mapper.RumorMapper;
import com.xiaoyao.opjo.model.Rumor;
import com.xiaoyao.service.RumorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: RumorServiceImpl
 * @Description: TODO(RumorServiceImpl)
 * @Author: WDD
 * @Date: 2020/2/26 22:33
 */
@Service
public class RumorServiceImpl implements RumorService {

    @Autowired
    private RumorMapper mapper;

    @Override
    public List<Rumor> findAll() {
        return mapper.findAll();
    }

    @Override
    public boolean add(Rumor rumor) {
        rumor.setCreateBy("SYSTEM");
        rumor.setUpdateBy("SYSTEM");
        return mapper.add(rumor);
    }
}
