package com.xiaoyao.service;

import com.xiaoyao.opjo.model.Rumor;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: RumorService
 * @Description: TODO(RumorService)
 * @Author: WDD
 * @Date: 2020/2/26 22:33
 */
public interface RumorService {

    List<Rumor> findAll();


    boolean add(Rumor rumor);

}
