package com.xiaoyao.service;

import com.xiaoyao.opjo.model.DynamicInformation;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: DynamicInformationService
 * @Description: TODO(DynamicInformationService接口)
 * @Author: WDD
 * @Date: 2020/2/26 18:34
 */
public interface DynamicInformationService{

    List<DynamicInformation> findAll();

    boolean add(DynamicInformation di);

}
