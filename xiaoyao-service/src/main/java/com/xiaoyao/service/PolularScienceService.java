package com.xiaoyao.service;

import com.xiaoyao.opjo.model.PolularScience;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: PolularScienceService
 * @Description: TODO(PolularScienceService)
 * @Author: WDD
 * @Date: 2020/2/26 21:29
 */
public interface PolularScienceService {

    List<PolularScience> findAll();


    boolean add(PolularScience ps);

}
