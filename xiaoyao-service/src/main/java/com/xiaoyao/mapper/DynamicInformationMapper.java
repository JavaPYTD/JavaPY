package com.xiaoyao.mapper;

import com.xiaoyao.opjo.model.DynamicInformation;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: DynamicInformationMapper
 * @Description: TODO(官方动态Mapper)
 * @Author: WDD
 * @Date: 2020/2/26 18:03
 */
@Mapper
@Repository
public interface DynamicInformationMapper {


    List<DynamicInformation> findAll();


    boolean add(DynamicInformation di);


}
