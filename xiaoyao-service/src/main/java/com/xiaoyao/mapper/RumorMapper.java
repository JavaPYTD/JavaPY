package com.xiaoyao.mapper;

import com.xiaoyao.opjo.model.Rumor;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: RumorMapper
 * @Description: TODO(RumorMapper)
 * @Author: WDD
 * @Date: 2020/2/26 22:25
 */
@Mapper
@Repository
public interface RumorMapper {

    List<Rumor> findAll();


    boolean add(Rumor rumor);

}
