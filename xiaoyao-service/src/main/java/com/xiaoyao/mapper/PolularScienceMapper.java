package com.xiaoyao.mapper;

import com.xiaoyao.opjo.model.PolularScience;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: PolularScienceMapper
 * @Description: TODO(一句话描述该类的功能)
 * @Author: WDD
 * @Date: 2020/2/26 21:31
 */
@Mapper
@Repository
public interface PolularScienceMapper {


    List<PolularScience> findAll();


    boolean add(PolularScience ps);


}
