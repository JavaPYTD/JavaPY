package com.xiaoyao.opjo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: PolularScience
 * @Description: TODO(科普实体类)
 * @Author: WDD
 * @Date: 2020/2/26 20:33
 */
@Alias("PolularScience")
@ApiModel("科普POJO")
public class PolularScience {

    @ApiModelProperty("主键,无实意")
    private Long id;//主键，无实意
    @ApiModelProperty("标题")
    private String psTitle;//标题
    @ApiModelProperty("图片来源地址")
    private String psImgSrc;//图片来源地址
    @ApiModelProperty("科普内容")
    private String psContext;//科普内容
    @ApiModelProperty("信息来源地址")
    private String psInfoSrc;//信息来源地址
    @ApiModelProperty("信息来源")
    private String psInfoFrom;//信息来源
    @ApiModelProperty("发布时间")
    private Date releaseTime;//发布时间
    @ApiModelProperty("点赞次数")
    private Integer stickCount;//点赞次数
    @ApiModelProperty("收藏次数")
    private Integer collectionCount;//收藏次数
    @ApiModelProperty("点击量")
    private Integer clicksCount;//点击量
    @ApiModelProperty("创建时间")
    private Date createTime;//创建时间
    @ApiModelProperty("创建者")
    private String createBy;//创建者
    @ApiModelProperty("更新时间")
    private Date updateTime;//跟新时间
    @ApiModelProperty("更新者")
    private String updateBy;//跟新者

    public String getPsInfoFrom() {
        return psInfoFrom;
    }

    public void setPsInfoFrom(String psInfoFrom) {
        this.psInfoFrom = psInfoFrom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPsTitle() {
        return psTitle;
    }

    public void setPsTitle(String psTitle) {
        this.psTitle = psTitle;
    }

    public String getPsImgSrc() {
        return psImgSrc;
    }

    public void setPsImgSrc(String psImgSrc) {
        this.psImgSrc = psImgSrc;
    }

    public String getPsContext() {
        return psContext;
    }

    public void setPsContext(String psContext) {
        this.psContext = psContext;
    }

    public String getPsInfoSrc() {
        return psInfoSrc;
    }

    public void setPsInfoSrc(String psInfoSrc) {
        this.psInfoSrc = psInfoSrc;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public Integer getStickCount() {
        return stickCount;
    }

    public void setStickCount(Integer stickCount) {
        this.stickCount = stickCount;
    }

    public Integer getCollectionCount() {
        return collectionCount;
    }

    public void setCollectionCount(Integer collectionCount) {
        this.collectionCount = collectionCount;
    }

    public Integer getClicksCount() {
        return clicksCount;
    }

    public void setClicksCount(Integer clicksCount) {
        this.clicksCount = clicksCount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
}
