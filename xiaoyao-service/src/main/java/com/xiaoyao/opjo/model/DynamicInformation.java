package com.xiaoyao.opjo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: DynamicInformation
 * @Description: TODO(官方动态实体类)
 * @Author: WDD
 * @Date: 2020/2/26 17:36
 */
@Alias("DynamicInformation")
@ApiModel("官方动态POJO")
public class DynamicInformation {

    @ApiModelProperty("主键，无实意")
    private Long id;//主键，无实意
    @ApiModelProperty("标题")
    private String diTitle;//标题
    @ApiModelProperty("信息来源地址")
    private String diInfoSrc;//信息来源地址
    @ApiModelProperty("信息来源")
    private String diInfoFrom;//信息来源
    @ApiModelProperty("图片来源地址")
    private String diImgSrc;//图片来源地址
    @ApiModelProperty("内容")
    private String diContext;//内容
    @ApiModelProperty("发布时间")
    private Date releaseTime;//发布时间
    @ApiModelProperty("点赞次数")
    private Integer stickCount;//点赞次数
    @ApiModelProperty("收藏次数")
    private Integer collectionCount;//收藏次数
    @ApiModelProperty("点击量")
    private Integer clicksCount;//点击量
    @ApiModelProperty("创建时间")
    private Date createTime;//创建时间
    @ApiModelProperty("创建者")
    private String createBy;//创建者
    @ApiModelProperty("更新时间")
    private Date updateTime;//跟新时间
    @ApiModelProperty("更新者")
    private String updateBy;//跟新者

    public String getDiInfoFrom() {
        return diInfoFrom;
    }

    public void setDiInfoFrom(String diInfoFrom) {
        this.diInfoFrom = diInfoFrom;
    }

    public Integer getStickCount() {
        return stickCount;
    }

    public void setStickCount(Integer stickCount) {
        this.stickCount = stickCount;
    }

    public Integer getCollectionCount() {
        return collectionCount;
    }

    public void setCollectionCount(Integer collectionCount) {
        this.collectionCount = collectionCount;
    }

    public Integer getClicksCount() {
        return clicksCount;
    }

    public void setClicksCount(Integer clicksCount) {
        this.clicksCount = clicksCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiTitle() {
        return diTitle;
    }

    public void setDiTitle(String diTitle) {
        this.diTitle = diTitle;
    }

    public String getDiInfoSrc() {
        return diInfoSrc;
    }

    public void setDiInfoSrc(String diInfoSrc) {
        this.diInfoSrc = diInfoSrc;
    }

    public String getDiImgSrc() {
        return diImgSrc;
    }

    public void setDiImgSrc(String diImgSrc) {
        this.diImgSrc = diImgSrc;
    }

    public String getDiContext() {
        return diContext;
    }

    public void setDiContext(String diContext) {
        this.diContext = diContext;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
}
