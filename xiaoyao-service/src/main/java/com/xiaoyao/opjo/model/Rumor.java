package com.xiaoyao.opjo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: Rumor
 * @Description: TODO(谣言实体类)
 * @Author: WDD
 * @Date: 2020/2/26 22:17
 */
@Alias("Rumor")
@ApiModel("谣言POJO")
public class Rumor {

    @ApiModelProperty("主键,无实意")
    private Long id;//主键，无实意
    @ApiModelProperty("结论")
    private String rConclusion;//结论
    @ApiModelProperty("标题")
    private String rTitle;//标题
    @ApiModelProperty("信息来源地址")
    private String rInfoSrc;//信息来源地址
    @ApiModelProperty("信息来源")
    private String rInfoFrom;//信息来源
    @ApiModelProperty("图片来源地址")
    private String rImgSrc;//图片来源地址
    @ApiModelProperty("内容")
    private String rContext;//内容
    @ApiModelProperty("发布时间")
    private Date releaseTime;//发布时间
    @ApiModelProperty("点赞次数")
    private Integer stickCount;//点赞次数
    @ApiModelProperty("收藏次数")
    private Integer collectionCount;//收藏次数
    @ApiModelProperty("点击量")
    private Integer clicksCount;//点击量
    @ApiModelProperty("创建时间")
    private Date createTime;//创建时间
    @ApiModelProperty("创建者")
    private String createBy;//创建者
    @ApiModelProperty("更新时间")
    private Date updateTime;//跟新时间
    @ApiModelProperty("更新者")
    private String updateBy;//跟新者


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getrConclusion() {
        return rConclusion;
    }

    public void setrConclusion(String rConclusion) {
        this.rConclusion = rConclusion;
    }

    public String getrTitle() {
        return rTitle;
    }

    public void setrTitle(String rTitle) {
        this.rTitle = rTitle;
    }

    public String getrInfoSrc() {
        return rInfoSrc;
    }

    public void setrInfoSrc(String rInfoSrc) {
        this.rInfoSrc = rInfoSrc;
    }

    public String getrInfoFrom() {
        return rInfoFrom;
    }

    public void setrInfoFrom(String rInfoFrom) {
        this.rInfoFrom = rInfoFrom;
    }

    public String getrImgSrc() {
        return rImgSrc;
    }

    public void setrImgSrc(String rImgSrc) {
        this.rImgSrc = rImgSrc;
    }

    public String getrContext() {
        return rContext;
    }

    public void setrContext(String rContext) {
        this.rContext = rContext;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public Integer getStickCount() {
        return stickCount;
    }

    public void setStickCount(Integer stickCount) {
        this.stickCount = stickCount;
    }

    public Integer getCollectionCount() {
        return collectionCount;
    }

    public void setCollectionCount(Integer collectionCount) {
        this.collectionCount = collectionCount;
    }

    public Integer getClicksCount() {
        return clicksCount;
    }

    public void setClicksCount(Integer clicksCount) {
        this.clicksCount = clicksCount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
}
