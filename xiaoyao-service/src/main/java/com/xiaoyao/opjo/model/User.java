package com.xiaoyao.opjo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * @version v1.0
 * @ProjectName: xiaoyao-service
 * @ClassName: User
 * @Description: TODO(用户实体类)
 * @Author: WDD
 * @Date: 2020/2/26 17:51
 */
@Alias("User")
@ApiModel("用户POJO")
public class User {

    @ApiModelProperty("主键，无实意")
    private Long id;//主键，无实意
    @ApiModelProperty("昵称")
    private String nickName;//昵称
    @ApiModelProperty("头像地址")
    private String avatarUrl;//头像地址
    @ApiModelProperty("性别")
    private int gender;//性别
    @ApiModelProperty("国家")
    private String country;//国家
    @ApiModelProperty("省份")
    private String province;//省份
    @ApiModelProperty("城市")
    private String city;//城市
    @ApiModelProperty("语言")
    private String language;//语言
    @ApiModelProperty("唯一标识")
    private String openid;//唯一标识
    @ApiModelProperty("创建时间")
    private Date createTime;//创建时间
    @ApiModelProperty("创建者")
    private String createBy;//创建者
    @ApiModelProperty("更新时间")
    private Date updateTime;//跟新时间
    @ApiModelProperty("更新者")
    private String updateBy;//跟新者


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
}
